const request = require('supertest')
const app = require('../../../app')
const { User } = require('../../../app/models')

describe('POST /v1/auth/register', () => {
  // it('should response with 201 as status code', async () => {
  //   const name = 'coba9'
  //   const email = 'coba9@binar.co.id'
  //   const password = '123456'

  //   return request(app)
  //     .post('/v1/auth/register')
  //     .set('Content-Type', 'application/json')
  //     .send({ name, email, password })
  //     .then((res) => {
  //       expect(res.statusCode).toBe(201)
  //       expect(res.body).toEqual(
  //         expect.objectContaining({
  //           ...res.body,
  //           accessToken: expect.any(String),
  //         }),
  //       )
  //     })
  // })

  it('should response with 422 as status code', async () => {
    const name = 'coba1'
    const email = 'coba1@binar.co.id'
    const password = '123456'

    return request(app)
      .post('/v1/auth/register')
      .set('Content-Type', 'application/json')
      .send({ name, email, password })
      .then((res) => {
        expect(res.statusCode).toBe(500)
        expect(res.body).toEqual(
          expect.objectContaining({
            error: expect.objectContaining({
              name: expect.any(String),
              message: expect.any(String),
              details: expect.any(Object),
            }),
          }),
        )
      })
  })
})
