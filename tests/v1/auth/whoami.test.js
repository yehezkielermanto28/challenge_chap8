const request = require('supertest')
const app = require('../../../app')

let token

beforeEach(async () => {
  const response = await request(app)
    .post('/v1/auth/login')
    .set('Content-Type', 'application/json')
    .send({ email: 'johnny@binar.co.id', password: '123456' })
  token = response.body.accessToken
})

describe('GET /v1/auth/whoami', () => {
  test('should response with 201 as status code', async () => {
    return request(app)
      .get('/v1/auth/whoami')
      .set({ Authorization: `Bearer ${token}` })
      .then((res) => {
        expect(res.statusCode).toBe(200)
        expect(res.body).toEqual(expect.any(Object))
      })
  })
})
