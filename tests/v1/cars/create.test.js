const request = require('supertest')
const app = require('../../../app')

let token
const name = 'Avanza'
const price = 200000
const size = 'SMALL'
const image = 'https://source.unsplash.com/500x500'

beforeEach(async () => {
  const response = await request(app)
    .post('/v1/auth/login')
    .set('Content-Type', 'application/json')
    .send({ email: 'ranggawarsita@binar.co.id', password: '123456' })
  token = response.body.accessToken
})

describe('POST /v1/cars', () => {
  test('should response with 201 as status code', async () => {
    return request(app)
      .post('/v1/cars')
      .set({ Authorization: `Bearer ${token}` })
      .send({ name, price, size, image })
      .then((res) => {
        expect(res.statusCode).toBe(201)
        expect(res.body).toEqual(expect.any(Object))
      })
  })

  test('should response with 422 as status code', async () => {
    const name = {}
    const price = {}
    const size = {}
    const image = {}
    return request(app)
      .post('/v1/cars')
      .set({ Authorization: `Bearer ${token}` })
      .send({ name, price, size, image })
      .then((res) => {
        expect(res.statusCode).toBe(422)
        expect(res.body).toEqual(
          expect.objectContaining({
            error: expect.objectContaining({
              name: expect.any(String),
              message: expect.any(String),
            }),
          }),
        )
      })
  })
})
