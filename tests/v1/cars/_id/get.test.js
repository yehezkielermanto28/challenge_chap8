const request = require('supertest')
const app = require('../../../../app')

describe('GET /v1/cars/:id', () => {
  it('should response with 200 as status code', async () => {
    const id = 1
    return request(app)
      .get('/v1/cars/' + id)
      .then((res) => {
        expect(res.statusCode).toBe(200)
        expect(res.body).toEqual(expect.any(Object))
      })
  })
})
