const request = require('supertest')
const app = require('../../../../app')

let token
const id = 1

beforeEach(async () => {
  const response = await request(app)
    .post('/v1/auth/login')
    .set('Content-Type', 'application/json')
    .send({ email: 'johnny@binar.co.id', password: '123456' })
  token = response.body.accessToken
})

describe('POST /v1/cars/:id/rent', () => {
  test('should response with 201 as status code', async () => {
    return request(app)
      .post(`/v1/cars/${id}/rent`)
      .set({ Authorization: `Bearer ${token}` })
      .send({
        userId: 58,
        carId: 3,
        rentStartedAt: '2022-06-09 16:05:53.015 +00:00',
        rentEndedAt: '2022-06-10 16:05:53.015 +00:00',
      })
      .then((res) => {
        expect(res.statusCode).toBe(201)
        expect(res.body).toEqual(expect.any(Object))
      })
  })
})
