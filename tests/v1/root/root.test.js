const request = require('supertest')
const app = require('../../../app')

describe('GET /', () => {
  it('should response with 200 as status code', async () => {
    return request(app)
      .get('/')
      .then((res) => {
        expect(res.statusCode).toBe(200)
        expect(res.body).toEqual(
          expect.objectContaining({
            status: expect.stringContaining('OK'),
            message: expect.stringContaining('BCR API is up and running!'),
          }),
        )
      })
  })
})
